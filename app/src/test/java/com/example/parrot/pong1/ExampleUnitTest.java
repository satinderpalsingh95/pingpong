package com.example.parrot.pong1;

import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    Racket racket = new Racket(350, 1200);
    @Test
    public void tc1(){
        int x = racket.getxRacketPosition();
        assertEquals(x,350);

    }



    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }

}